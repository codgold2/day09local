public class Food
{
    private String description;
    private int calories;
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public int getCalories()
    {
        return calories;
    {
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public void setCalories(int calories)
    {
        this.calories = calories;
    }
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
    * Set the description
    * @param inDescription is the new description
    */
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    
    @Override
    public String toString()
    {
        return "Somebody brought " + getDescription();
    }
    
}